from django.db.models import Sum
from django.shortcuts import render
from django.db.models import Q
from django.http import JsonResponse

import requests

from transactions.models import Diposit, Withdrawal, Interest, Transfer


def home(request):
    if not request.user.is_authenticated:
        return render(request, "core/home.html", {})
    else:
        user = request.user
        transfers = Transfer.objects.filter(Q(sender=str(user)) | Q(receiver=str(user)))
        deposit = Diposit.objects.filter(user=user)
        deposit_sum = deposit.aggregate(Sum('amount'))['amount__sum']
        withdrawal = Withdrawal.objects.filter(user=user)
        withdrawal_sum = withdrawal.aggregate(Sum('amount'))['amount__sum']
        interest = Interest.objects.filter(user=user)
        interest_sum = interest.aggregate(Sum('amount'))['amount__sum']

        if request.method == 'POST':
            currency = request.POST.get('selectedCurrency', 'error')
            curreArrat = currency.split(' ')
            r = requests.get('https://www.divipay.com/secure/currency/'+curreArrat[0]+'/')
            jsonResponseCurr = r.json()
            stringResult = float(user.balance) * float(jsonResponseCurr['rate'])

            return JsonResponse({'newCurrency':stringResult, 'sign': curreArrat[0]})


        context = {
                    "user": user,
                    "transfer": transfers,
                    "deposit": deposit,
                    "deposit_sum": deposit_sum,
                    "withdrawal": withdrawal,
                    "withdrawal_sum": withdrawal_sum,
                    "interest": interest,
                    "interest_sum": interest_sum,
                  }

        return render(request, "core/transactions.html", context)


def about(request):
    return render(request, "core/about.html", {})
