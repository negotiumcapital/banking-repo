$( document ).ready(function() {
    $('#currency').on('change', function(){
    	var selectedCurrency = $( "#currency option:selected" ).text();
    	var csfr_token = $("input[name='csrfmiddlewaretoken']").val();

    	function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
		    }

	    $.ajaxSetup({
	        beforeSend: function(xhr, settings) {
	            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
	                xhr.setRequestHeader("X-CSRFToken", csfr_token);
	            }
	        }
	    });

		$.ajax({
	        url: "/",
	        type: "POST",
	        data: { selectedCurrency: selectedCurrency },
	        success: function(data) {
	        	console.log(data.newCurrency);
                $("#balanceCur").html(data.newCurrency.toString() + ' ' + data.sign);
            },
            error: function(e) {
                console.log(e);
            }
    	});

    });
});