# Online Banking Repo 

This banking system is based on the following repo https://github.com/saadmk11/banking-system


to set up the app run

Python 3.6

set up virtual envoirnment and install requirments.txt

python manage.py migrate

python manage.py runserver

create account
create 2nd account and you're able to transfer money between accounts

## Features
* Create Bank Account.
* Load account Details Using Account Number & password.
* Deposit & Withdraw Money.
* Transaction Detail Page.
* Count Monthly Interest Using Celery.

## Features added
* Transfer of money between accounts
* Integration with a currency exchange API which converts any currency into Australian dollars see home section
* 100 dollars are in the bank account once it's created
* source and destination of all transfers is stored
* request of transfers in any currency has not been tackeled as it was a bit confusing

## Contact
name: Tobias Mahnert
Phone: 0411262089
Email: tobiasmahnert@web.de