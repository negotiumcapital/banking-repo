from django import forms

from .models import Diposit, Withdrawal, Transfer


class DepositForm(forms.ModelForm):
    class Meta:
        model = Diposit
        fields = ["amount"]


class WithdrawalForm(forms.ModelForm):
    class Meta:
        model = Withdrawal
        fields = ["amount"]

class TransferForm(forms.ModelForm):
    class Meta:
        model = Transfer
        fields = ["receiver","amount"]
